# esap-userprofile-python-client

A Python client for the ESCAPE ESAP User Profile REST API.

The `ShoppingClient` class, which communicates with the ESCAPE ESAP User Profile REST API, is very lightweight. Archive-specific functionality is delegated to "connector" classes like `Zooniverse` and `Alta`.

### Installation

The client and a selection of connector classes client cat be installed using pip:

```sh
$ pip install git+https://git.astron.nl/astron-sdc/esap-userprofile-python-client.git
```

### Example

See `example.py`.

## Contributing

For developer access to this repository, please send a message on the [ESAP channel on Rocket Chat](https://chat.escape2020.de/channel/esap).
